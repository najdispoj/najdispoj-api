# Map
bbox = ({'lat': 47.9721, 'lng': 16.8665}, {'lat': 48.3263, 'lng': 17.3557})
center_lat = '48.153932'
center_lng = '17.121921'

# OpenTripPlanner
otp_folder = '/path/to/opentripplanner/base/dir'
otp_cmd = 'java -Xmx1G -jar /path/to/opentripplanner/otp-1.5.0-shaded.jar '
otp_build_cmd = otp_cmd + ' --build ' + otp_folder + '/graphs/ba'
otp_run_cmd = otp_cmd + \
    ' --server --port 25515 --basePath ' + otp_folder + ' --router ba'

# OpenStreetMap data
osm_path = '/path/to/osm'
osm_raw_path = osm_path + '/raw.osm'
osm_altered_path = osm_path + '/altered.osm'
osm_filtered_path = otp_folder + '/graphs/ba/filtered.pbf'

# GTFS data
gtfs_host = ''
gtfs_username = ''
gtfs_password = ''

gtfs_folder = ''
gtfs_raw_folder = gtfs_folder + '/raw/'
# don't include ".zip" extension
gtfs_latest_zip = otp_folder + '/graphs/ba/latest'
pfaedle_run_cmd = '/path/to/pfaedle/build/pfaedle -D --inplace ' + \
    '-c /path/to/pfaedle/pfaedle.cfg -x ' + osm_raw_path + ' '

# Najdispoj API
api_run_cmd = 'python3 /path/to/api.py'
api_port = 25517
api_cert_path = '/path/to/cert.pem'
api_key_path = '/path/to/privkey.pem'
